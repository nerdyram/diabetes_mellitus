# Diabetes Mellitus

## Introduction

In patients with type I or type II diabetes, `beta(β) cell` mass and function are 
diminished, leading to insufficient `insulin secretion` and `hyperglycemia`.

## Medical Terms

- `Pathogenesis` is the biological mechanism (or mechanisms) that leads to a 
**diseased** state. The term can also be described as the origin and development 
of the disease, and whether it is `acute`, `chronic`, or `recurrent`.

- `Tachypnea` is a condition that refers to rapid breathing. The normal breathing 
rate for an average adult is 12 to 20 breaths per minute. In children, the 
number of breaths per minute can be a higher resting rate than seen in adults.

- `Beta cells (β cells)` are a type of cell found in `pancreatic` islets that 
synthesize and secrete `insulin` and `amylin`. Beta cells make up `50–70%` of the 
cells in human islets.



## References

[1] http://www.meddean.luc.edu/lumen/meded/mech/cases/case17/Case_f.htm

[2] https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4226760/